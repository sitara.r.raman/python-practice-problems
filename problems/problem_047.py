# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    lower_count = 0
    upper_count = 0
    digit_count = 0
    special_count = 0
    for letter in password:
        if (len(password) >= 6 and len(password) <= 12):
            if (letter.isupper()):
                upper_count += 1
            if (letter.islower()):
                lower_count += 1
            if (letter.isdigit()):
                digit_count += 1
            if (not letter.isalnum()):
                special_count += 1
    if (lower_count >= 1 and upper_count >=1 and digit_count >= 1 and special_count >= 1):
        return "The password is valid"
    return "The password is not valid"

test = "Password@1234"
test1 = "Pass@$1"
print(check_password(test))
print(check_password(test1))
