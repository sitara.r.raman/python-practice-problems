# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    ing_count = 0
    for ingredient in ingredients:
        if (ingredient == "flour" or ingredient == "eggs" or ingredient == "oil"):
            ing_count += 1
        if (ing_count >= 3):
            return True
        else:
            return False
