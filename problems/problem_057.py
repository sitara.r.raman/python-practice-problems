# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

from fractions import Fraction
def sum_fraction_sequence(limit):
    numerator = 1
    denominator = 2
    frac_sum = 0
    for num in range (0,limit):
        #print(Fraction(frac_sum).limit_denominator() , "+" , num+numerator, "/" , num+denominator, "=")
        frac_sum = frac_sum + ((num+numerator)/(num+denominator))
        #print(Fraction(frac_sum).limit_denominator())
    return Fraction(frac_sum).limit_denominator()

print(sum_fraction_sequence(2))
print(sum_fraction_sequence(3))
