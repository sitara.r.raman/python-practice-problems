# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    gear_list = ["Umbrella", "Laptop", "Surfboard"]
    if (is_workday == True and is_sunny == False):
        gear_list.remove("Surfboard")
        return gear_list
    elif (is_workday == True and is_sunny == True):
        return gear_list
    elif (is_workday == False and is_sunny == False):
        gear_list.remove("Laptop")
        gear_list.remove("Surfboard")
        return gear_list
    elif (is_workday == False and is_sunny == True):
        gear_list.remove("Laptop")
        gear_list.remove("Umbrella")
        return gear_list
