# Write a function that meets these requirements.
#
# Name:       simple_roman
# Parameters: one parameter that has a value from 1
#             to 10, inclusive
# Returns:    the Roman numeral equivalent of the
#             parameter value
#
# All examples
#     * input: 1
#       returns: "I"
#     * input: 2
#       returns: "II"
#     * input: 3
#       returns: "III"
#     * input: 4
#       returns: "IV"
#     * input: 5
#       returns: "V"
#     * input: 6
#       returns: "VI"
#     * input: 7
#       returns: "VII"
#     * input: 8
#       returns: "VIII"
#     * input: 9
#       returns: "IX"
#     * input: 10
#       returns:  "X"

def simple_roman(num):
    if (num >= 11):
        raise ValueError
    if (num >= 1 and num <= 3):
        roman = ""
        for i in range(1, num+1):
            roman += "I"
    elif (num == 4):
        roman = "IV"
    elif (num == 5):
        roman = "V"
    elif (num >= 6 and num <= 8):
        roman = "V"
        for i in range(1, num-4):
            roman += "I"
    elif (num == 9):
        roman = "IX"
    elif (num == 10):
        roman = "X"
    return roman

print(simple_roman(5))
print (simple_roman(6))
print(simple_roman(1))
print(simple_roman(3))
print(simple_roman(2))
