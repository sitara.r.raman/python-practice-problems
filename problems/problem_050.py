# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]


def halve_the_list(numlist):
    first_half_list = []
    second_half_list = []

    for num in range(0,len(numlist)):
        if (num <= (len(numlist)-1)/2):
            first_half_list.append(numlist[num])
            #print("first half list:" , first_half_list)
        else:
            second_half_list.append(numlist[num])
            #print("second half list:" , second_half_list)
    return first_half_list, second_half_list

testlist = [1,2,3,4,5]
testlist1 = [1,2,3,4]
print(halve_the_list(testlist))
print(halve_the_list(testlist1))
