# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    i = len(word)-1
    rev_word = ""
    while i > -1:
        rev_word = rev_word + word[i]
        i = i-1
    if (rev_word == word):
        return "Is a palindrome"
    else:
        return "Is not a palindrome"
    return "Error"
