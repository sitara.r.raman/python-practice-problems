# Write four classes that meet these requirements.
#
# Name:       Animal
#
# Required state:
#    * number_of_legs, the number of legs the animal has
#    * primary_color, the primary color of the animal
#
# Behavior:
#    * describe()       # Returns a string that describes that animal
#                         in the format
#                                self.__class__.__name__
#                                + " has "
#                                + str(self.number_of_legs)
#                                + " legs and is primarily "
#                                + self.primary_color
#
#
# Name:       Dog, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Bark!"
#
#
#
# Name:       Cat, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Miao!"
#
#
#
# Name:       Snake, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Sssssss!"


class Animal:
    def __init__(self, legs, colour):
        self.number_of_legs = legs
        self.primary_colour = colour
    def describe(self):
        return f"{self.__class__.__name__} has {str(self.number_of_legs)} legs and is primarily {self.primary_colour}"

class Cat(Animal):
    def __init__(self, colour):
        super().__init(4, colour)
    def speak(self):
        return "Meow"

class Dog(Animal):
    def __init__(self, colour):
        super().__init(4, colour)
    def speak(self):
        return "Bark"

class Snake(Animal):
    def __init__(self, colour):
        super().__init(0, colour)
    def speak(self):
        return "Hiss"
